import {NgModule} from '@angular/core';
import {MaterialsModule} from './materials/materials.module';


@NgModule({
  declarations: [],
  imports: [],
  exports: [
    MaterialsModule
  ]
})
export class ThemeLibModule {
}
