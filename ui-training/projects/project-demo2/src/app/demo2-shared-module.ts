import {ModuleWithProviders, NgModule} from '@angular/core';
import {AppModule} from './app.module';

@NgModule({})
export class Demo2SharedModule {
    static forRoot(): ModuleWithProviders<any> {
        return {
            ngModule: AppModule,
            providers: []
        };
    }
}
