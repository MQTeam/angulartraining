import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {LayoutComponent} from './layout/layout.component';
import {Page1Component} from './page1/page1.component';
import {Page2Component} from './page2/page2.component';
import {HashLocationStrategy, LocationStrategy} from '@angular/common';
import {ThemeLibModule} from 'theme-lib';

@NgModule({
  declarations: [
    AppComponent,
    LayoutComponent,
    Page1Component,
    Page2Component
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ThemeLibModule
  ],
  providers: [
    {
      provide: LocationStrategy,
      useClass: HashLocationStrategy,
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
