import {Component, OnInit} from '@angular/core';
import {MealDataService, MealModel} from '../services/meal-data.service';

@Component({
  selector: 'app-menus',
  templateUrl: './menus.component.html',
  styleUrls: ['./menus.component.scss']
})
export class MenusComponent implements OnInit {
  meals: MealModel[] = [];

  constructor(private _MealDataService: MealDataService) {
  }

  async ngOnInit() {
    this.meals = await this._MealDataService.loadAllMeals()
  }

}
