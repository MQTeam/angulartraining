import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MealDataService {
  mealData: MealModel[];

  constructor() {
    this.mealData = [
      {
        id: 1,
        name: 'Ice scream',
      },
      {
        id: 2,
        name: 'Noodle',
      }
    ];
  }

  loadAllMeals() {
    return Promise.resolve(this.mealData);
  }
}

export class MealModel {
  id: number = -1;
  name: string = '';
}
