import {Injectable} from '@angular/core';
import * as _ from 'lodash';

@Injectable({
  providedIn: 'root'
})
export class DriverDataService {
  driverData: DriverModel[];
  orderData: OrderModel[];

  constructor() {
    this.orderData = [
      {
        no: 1,
        description: '',
        items: ['Hamburger', 'Ice cream']
      },
      {
        no: 2,
        description: '',
        items: ['Hamburger', 'Ice cream']
      },
      {
        no: 3,
        description: '',
        items: ['Hamburger', 'Ice cream']
      },
      {
        no: 4,
        description: '',
        items: ['Hamburger', 'Ice cream']
      }
    ]
    this.driverData = [
      {
        id: 1,
        name: 'Driver A',
        orderNos: [1, 2]
      },
      {
        id: 2,
        name: 'Driver B',
        orderNos: [3, 4]
      }
    ];

  }

  loadAllDrivers() {
    return Promise.resolve(this.driverData);
  }

  loadDriverById(id: number) {
    return Promise.resolve(_.find(this.driverData, x => x.id === id))
      .then((rs) => {
        console.log(rs);
        return rs;
      });
  }

  loadDriverByName(name: string) {
    return Promise.resolve(_.find(this.driverData, x => x.name === name));
  }

  loadOrderByNo(no: number) {
    return Promise.resolve(_.find(this.orderData, x => x.no === no));
  }

  loadOrdersByDriver(id: number) {
    return this.loadDriverById(id)
      .then((rs) => {
        return Promise.all(_.map(rs?.orderNos, x => this.loadOrderByNo(x)))
          .then((list) => {
            console.log(list);
            return list;
          });
      })
  }

}

export class DriverModel {
  id: number = -1;
  name: string = '';
  orders?: OrderModel[] = [];
  orderNos?: number[] = [];
}

export class OrderModel {
  no: number = -1;
  description: string = '';
  items?: string[] = [];
}
