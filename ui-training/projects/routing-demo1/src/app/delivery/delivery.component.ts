import {Component, OnInit} from '@angular/core';
import {DriverDataService, DriverModel} from '../services/driver-data.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-delivery',
  templateUrl: './delivery.component.html',
  styleUrls: ['./delivery.component.scss']
})
export class DeliveryComponent implements OnInit {
  drivers: DriverModel[] = [];
  selectedDriver: DriverModel | null = null;

  constructor(private _DriverDataService: DriverDataService,
              private _Router: Router,
              private _ActivatedRoute: ActivatedRoute) {
    this._ActivatedRoute.children[0]?.data.subscribe((rs) => {
      this.selectedDriver = rs?.driver as DriverModel | null;
    })
  }

  async ngOnInit() {
    this.drivers = await this._DriverDataService.loadAllDrivers();
  }

  selectDriver(d: DriverModel) {
    this.selectedDriver = d;
    this._Router.navigate(['drivers', d.id], {relativeTo: this._ActivatedRoute});
  }
}
