import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppComponent} from './app.component';
import {HashLocationStrategy, LocationStrategy} from '@angular/common';
import {AppRoutingModule} from './app-routing.module';
import {ThemeLibModule} from 'theme-lib';
import {DeliveryComponent} from './delivery/delivery.component';
import {DriverComponent} from './driver/driver.component';
import {OrderComponent} from './order/order.component';
import {NoPermissionComponent} from './no-permission/no-permission.component';
import {KeyValueOrderPipe} from './pipe/key-value-order.pipe';
import {ComponentLibModule} from 'component-lib';
import { MenusComponent } from './menus/menus.component';
import { MealComponent } from './meal/meal.component';

@NgModule({
  declarations: [
    AppComponent,
    DeliveryComponent,
    DriverComponent,
    OrderComponent,
    NoPermissionComponent,
    KeyValueOrderPipe,
    MenusComponent,
    MealComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ThemeLibModule,
    ComponentLibModule
  ],
  providers: [
    {
      provide: LocationStrategy,
      useClass: HashLocationStrategy,
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
