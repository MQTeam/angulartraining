import {Component, OnInit} from '@angular/core';
import {DriverModel, OrderModel} from '../services/driver-data.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-driver',
  templateUrl: './driver.component.html',
  styleUrls: ['./driver.component.scss']
})
export class DriverComponent implements OnInit {
  selectedDriver: DriverModel | null = null;
  selectedOrder: OrderModel | null = null;
  selectedOrderNo: number | undefined = -1;

  constructor(private _Router: Router,
              private _ActivatedRoute: ActivatedRoute) {
    this._ActivatedRoute.data.subscribe((rs) => {
      if (rs) {
        this.selectedDriver = rs?.driver as DriverModel;
      }
    })

    this._ActivatedRoute?.children[0]?.data.subscribe((rs) => {
      this.selectedOrder = rs?.order as OrderModel | null;
      this.selectedOrderNo = this.selectedOrder?.no;
    })
  }

  ngOnInit(): void {
  }

  selectOrder(o: number) {
    this.selectedOrderNo = o;
    this._Router.navigate(['orders', o], {relativeTo: this._ActivatedRoute});
  }
}
