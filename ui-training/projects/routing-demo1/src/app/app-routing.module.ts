import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {DeliveryComponent} from './delivery/delivery.component';
import {DriverComponent} from './driver/driver.component';
import {OrderComponent} from './order/order.component';
import {DriverResolver} from './resolvers/driver.resolver';
import {OrderResolver} from './resolvers/order.resolver';
import {ENUM_ROUTER_KEYS} from 'component-lib';
import {MenusComponent} from './menus/menus.component';
import {MealComponent} from './meal/meal.component';
import {MealResolver} from './resolvers/meal.resolver';

const routes: Routes = [
  {
    path: 'delivery',
    component: DeliveryComponent,
    data: {
      [ENUM_ROUTER_KEYS.BREADCRUMB]: 'Delivery',
    },
    children: [
      {
        path: 'drivers/:id',
        resolve: {
          driver: DriverResolver
        },
        data: {
          [ENUM_ROUTER_KEYS.BREADCRUMB]: 'Driver {id}',
          [ENUM_ROUTER_KEYS.BREADCRUMB_PARAM]: ['id'],
        },
        component: DriverComponent,
        children: [
          {
            path: 'orders/:id',
            data: {
              [ENUM_ROUTER_KEYS.BREADCRUMB]: 'Order {id}',
              [ENUM_ROUTER_KEYS.BREADCRUMB_PARAM]: ['id'],
            },
            resolve: {
              order: OrderResolver
            },
            component: OrderComponent
          }
        ]
      }
    ]
  },
  {
    path: 'menus',
    component: MenusComponent,
    data: {
      [ENUM_ROUTER_KEYS.BREADCRUMB]: 'Menus',
    },
    children: [
      {
        path: 'meal',
        children: [
          {
            path: '',
            redirectTo: 'null',
            pathMatch: 'full',
          },
          {
            path: ':id',
            resolve: {
              meal: MealResolver
            },
            component: MealComponent,
            data: {
              [ENUM_ROUTER_KEYS.BREADCRUMB]: 'Meal {id}',
              [ENUM_ROUTER_KEYS.BREADCRUMB_PARAM]: ['id'],
            },
          }
        ]
      }
    ]
  },
  {
    path: '',
    redirectTo: 'delivery',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
