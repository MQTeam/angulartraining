import {Injectable} from '@angular/core';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import {from, Observable, of} from 'rxjs';
import {DriverDataService, DriverModel} from '../services/driver-data.service';

@Injectable({
  providedIn: 'root'
})
export class DriverResolver implements Resolve<any> {
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot)
    : Observable<any> {
    if (route?.params && !!route?.params?.id) {
      return from(
        this._DriverDataService.loadDriverById(parseInt(route?.params?.id))
      );
    }
    return of(false);
  }

  constructor(
    private _DriverDataService: DriverDataService
  ) {

  }

}
