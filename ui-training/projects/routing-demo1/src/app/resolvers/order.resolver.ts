import {Injectable} from '@angular/core';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import {from, Observable, of} from 'rxjs';
import {DriverDataService} from '../services/driver-data.service';

@Injectable({
  providedIn: 'root'
})
export class OrderResolver implements Resolve<any> {
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
    if (route?.params && !!route?.params?.id) {
      return from(this._DriverDataService.loadOrderByNo(parseInt(route?.params?.id)));
    }
    return of(false);
  }

  constructor(
    private _DriverDataService: DriverDataService
  ) {

  }

}
