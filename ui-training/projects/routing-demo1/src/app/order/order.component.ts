import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {DriverModel, OrderModel} from '../services/driver-data.service';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss']
})
export class OrderComponent implements OnInit {
  selectedOrder: OrderModel | null = null;

  constructor(private _Router: Router,
              private _ActivatedRoute: ActivatedRoute) {
    this._ActivatedRoute.data.subscribe((rs) => {
      if (rs) {
        this.selectedOrder = rs?.order as OrderModel;
      }
    })
  }

  ngOnInit(): void {
  }

}
