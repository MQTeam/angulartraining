import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

const routes: Routes = [
  {
    path: 'demo1',
    loadChildren: () => import('../../../project-demo1/src/app/demo1-shared-module').then(m => m.Demo1SharedModule)
  },
  {
    path: 'demo2',
    loadChildren: () => import('../../../project-demo2/src/app/demo2-shared-module').then(m => m.Demo2SharedModule)
  },
  {
    path: '',
    redirectTo: 'demo1/home',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
