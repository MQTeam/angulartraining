import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {ThemeLibModule} from 'theme-lib';
import {HashLocationStrategy, LocationStrategy} from '@angular/common';
import {Demo1SharedModule} from '../../../project-demo1/src/app/demo1-shared-module';
import {Demo2SharedModule} from '../../../project-demo2/src/app/demo2-shared-module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ThemeLibModule,
    Demo1SharedModule.forRoot(),
    Demo2SharedModule.forRoot(),
  ],
  providers: [
    {
      provide: LocationStrategy,
      useClass: HashLocationStrategy,
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
