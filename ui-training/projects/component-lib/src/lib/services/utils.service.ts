import {Injectable} from '@angular/core';
import {DatePipe} from '@angular/common';
import * as _ from 'lodash';

// import * as xml2js from 'xml2js';

@Injectable({
  providedIn: 'root',
})
export class UtilsService {
  static _DatePipe = new DatePipe('en-US');

  constructor() {
  }

  static isObject(data: any) {
    return data?.constructor === ({}).constructor;
  }

  static isArray(data: any) {
    return data?.constructor === ([]).constructor;
  }

  /**
   * check if is array, if is not, return [value]
   * @param value: string
   * @returns any
   */
  static toArray(value: any) {
    return _.isArray(value) ? value : [value];
  }

  /**
   * replace all {find} inside {str} by {replace}
   * @param text: string
   * @param find: string
   * @param replace: string
   * @returns any
   */
  static replaceAll(text: string, find: any, replace: any): string {
    function escapeRegExp(str: string) {
      return str.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, '\\$1');
    }

    return text.replace(new RegExp(escapeRegExp(find), 'g'), replace);
  }
}
