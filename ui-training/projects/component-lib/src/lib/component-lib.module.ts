import {NgModule} from '@angular/core';
import {UiBreadcrumbComponent} from './ui-breadcrumb/ui-breadcrumb.component';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';

@NgModule({
  declarations: [
    UiBreadcrumbComponent
  ],
  imports: [
    RouterModule,
    CommonModule
  ],
  exports: [
    UiBreadcrumbComponent
  ]
})
export class ComponentLibModule {
}
