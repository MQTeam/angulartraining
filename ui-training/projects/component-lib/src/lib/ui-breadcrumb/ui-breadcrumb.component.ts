import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {filter} from 'rxjs/operators';
import {ENUM_ROUTER_KEYS, IRouterData} from '../interface/i-router-data';
import {UtilsService} from '../services/utils.service';
import * as _ from 'lodash';

export interface IBreadcrumb {
  label: string;
  url: string;
}

@Component({
  selector: 'ui-breadcrumb',
  templateUrl: './ui-breadcrumb.component.html',
  styleUrls: ['./ui-breadcrumb.component.scss']
})
export class UiBreadcrumbComponent implements OnInit {
  @Input() hasPrefix: boolean = false;
  breadcrumbList: IBreadcrumb[] = [];

  constructor(private _Router: Router,
              private _ActivatedRoute: ActivatedRoute) {
    this._Router.events.pipe(
      filter((event) => event instanceof NavigationEnd)
    )
      .subscribe(async () => {
        this.breadcrumbList
          = await this.createBreadcrumbs(this._ActivatedRoute.root);
      });
  }

  ngOnInit(): void {
  }

  /**
   * source
   * https://github.com/piotrkorlaga/demo-angular-breadcrumb/blob/master/src/app/breadcrumb/breadcrumb.component.ts
   * @param route
   * @param url
   * @param breadcrumbs
   * @returns {IBreadcrumb[]}
   */
  private async createBreadcrumbs(route: ActivatedRoute, url: string = '#', breadcrumbs: IBreadcrumb[] = []): Promise<IBreadcrumb[]> {
    const children: ActivatedRoute[] = route.children;

    if (!!children && children.length > 0) {
      _.map(children, async child => {

        // for (const child of children) {
        const routeURL: string = child.snapshot.url.map((segment) => segment.path).join('/');
        if (routeURL !== '') {
          url += `/${routeURL}`;
        }

        const data: IRouterData = child.snapshot.data;
        if (!!data[ENUM_ROUTER_KEYS.BREADCRUMB]) {
          let label = data[ENUM_ROUTER_KEYS.BREADCRUMB] as string;
          if (data[ENUM_ROUTER_KEYS.BREADCRUMB]) {
            const params = UtilsService.toArray(data[ENUM_ROUTER_KEYS.BREADCRUMB_PARAM]);
            _.map(params, (x) => {
              label = UtilsService.replaceAll(label, `{${x}}`, child.snapshot.params[x]);
            });
          }
          breadcrumbs.push({label: label, url});
        }

        return await this.createBreadcrumbs(child, url, breadcrumbs);
        // }
      })
    }

    return _.uniqBy(breadcrumbs, (x) => x.url);
  }

  goTo(url: string | undefined) {
    this._Router.navigateByUrl(url as string);
  }
}

