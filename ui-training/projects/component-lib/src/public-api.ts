/*
 * Public API Surface of component-lib
 */

export * from './lib/component-lib.module';
export * from './lib/ui-breadcrumb/ui-breadcrumb.component';
export * from './lib/interface/i-router-data';
