import { NgModule } from '@angular/core';
import { ServiceLibComponent } from './service-lib.component';



@NgModule({
  declarations: [
    ServiceLibComponent
  ],
  imports: [
  ],
  exports: [
    ServiceLibComponent
  ]
})
export class ServiceLibModule { }
