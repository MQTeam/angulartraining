import { TestBed } from '@angular/core/testing';

import { ServiceLibService } from './service-lib.service';

describe('ServiceLibService', () => {
  let service: ServiceLibService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ServiceLibService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
