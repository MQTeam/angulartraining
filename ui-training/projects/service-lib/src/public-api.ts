/*
 * Public API Surface of service-lib
 */

export * from './lib/service-lib.service';
export * from './lib/service-lib.component';
export * from './lib/service-lib.module';
