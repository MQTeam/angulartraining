import {NgModule} from '@angular/core';
import {PreloadAllModules, RouterModule, Routes} from '@angular/router';
import {MainLayoutComponent} from './layout/main-layout/main-layout.component';
import {ProjectsComponent} from './routes/projects/projects.component';
import {SuppliersComponent} from './routes/suppliers/suppliers.component';
import {InventoryComponent} from './routes/inventory/inventory.component';
import {ForecastComponent} from './routes/forecast/forecast.component';
import {NavbarControllerService} from './management/navbar-controller.service';
import {NavBarProjectsComponent} from './routes/projects/nav-bar-projects/nav-bar-projects.component';
import {NavBarForecastComponent} from './routes/forecast/nav-bar-forecast/nav-bar-forecast.component';

const routes: Routes = [
    {
        path: '', redirectTo: 'main', pathMatch: 'full'
    },
    {
        path: 'main',
        component: MainLayoutComponent,
        children: [
            {
                path: '', redirectTo: 'projects', pathMatch: 'full'
            },
            {
                path: 'projects',
                data: {
                    navbarComponent: NavBarProjectsComponent
                },
                resolve: {
                    navbar: NavbarControllerService
                },
                component: ProjectsComponent
            },
            {
                path: 'suppliers',
                data: {
                    navbarComponent: null
                },
                resolve: {
                    navbar: NavbarControllerService
                },
                component: SuppliersComponent
            },
            {
                path: 'inventory',
                data: {
                    navbarComponent: null
                },
                resolve: {
                    navbar: NavbarControllerService
                },
                component: InventoryComponent
            },
            {
                path: 'forecast',
                data: {
                    navbarComponent: NavBarForecastComponent
                },
                resolve: {
                    navbar: NavbarControllerService
                },
                component: ForecastComponent
            }
        ]
    },
    {path: '**', redirectTo: 'main', pathMatch: 'full'},
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes, {
            // enableTracing: true,
            useHash: true,
            preloadingStrategy: PreloadAllModules,
        }),
    ],
})
export class AppRoutingModule {
}
