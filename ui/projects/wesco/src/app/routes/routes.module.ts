import {NgModule} from '@angular/core';
import {CommonModule, HashLocationStrategy, LocationStrategy} from '@angular/common';

import {RoutesRoutingModule} from './routes-routing.module';
import {ProjectsComponent} from './projects/projects.component';
import {SuppliersComponent} from './suppliers/suppliers.component';
import {InventoryComponent} from './inventory/inventory.component';
import {ForecastComponent} from './forecast/forecast.component';
import { NavBarProjectsComponent } from './projects/nav-bar-projects/nav-bar-projects.component';
import { NavBarForecastComponent } from './forecast/nav-bar-forecast/nav-bar-forecast.component';

@NgModule({
  declarations: [ProjectsComponent, SuppliersComponent, InventoryComponent, ForecastComponent, NavBarProjectsComponent, NavBarForecastComponent],
  imports: [
    CommonModule,
    RoutesRoutingModule
  ],
  exports: [
    RoutesRoutingModule
  ],
  providers: [
    {
      provide: LocationStrategy,
      useClass: HashLocationStrategy,
    }
  ]
})
export class RoutesModule {
}
