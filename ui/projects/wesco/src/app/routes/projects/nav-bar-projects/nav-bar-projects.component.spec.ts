import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NavBarProjectsComponent } from './nav-bar-projects.component';

describe('NavBarProjectsComponent', () => {
  let component: NavBarProjectsComponent;
  let fixture: ComponentFixture<NavBarProjectsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NavBarProjectsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NavBarProjectsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
