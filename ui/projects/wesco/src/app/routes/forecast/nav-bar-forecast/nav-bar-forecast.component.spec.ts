import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NavBarForecastComponent } from './nav-bar-forecast.component';

describe('NavBarForecastComponent', () => {
  let component: NavBarForecastComponent;
  let fixture: ComponentFixture<NavBarForecastComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NavBarForecastComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NavBarForecastComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
