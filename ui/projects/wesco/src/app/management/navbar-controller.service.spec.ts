import { TestBed } from '@angular/core/testing';

import { NavbarControllerService } from './navbar-controller.service';

describe('NavbarControllerService', () => {
  let service: NavbarControllerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NavbarControllerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
