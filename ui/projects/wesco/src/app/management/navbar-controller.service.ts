import {Component, Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {ActivatedRouteSnapshot, Data, Resolve, RouterStateSnapshot} from '@angular/router';

export interface IRouterData extends Data {
    navbarComponent: Component;
}

@Injectable({
    providedIn: 'root'
})
export class NavbarControllerService implements Resolve<any> {
    __Observable_Component: BehaviorSubject<Component> = new BehaviorSubject<Component>(null);

    constructor() {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        const data: IRouterData = route.data as IRouterData;
        const navbarComponent = data.navbarComponent;

        if (navbarComponent !== this.__Observable_Component.value) {
            this.__Observable_Component.next(navbarComponent);
        }

    }
}
