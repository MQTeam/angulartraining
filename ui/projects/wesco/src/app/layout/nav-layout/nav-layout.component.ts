import {Component, OnInit} from '@angular/core';
import {NavbarControllerService} from '../../management/navbar-controller.service';

@Component({
    selector: 'app-nav-layout',
    templateUrl: './nav-layout.component.html',
    styleUrls: ['./nav-layout.component.scss']
})
export class NavLayoutComponent implements OnInit {
    navBarComponent: Component;

    constructor(private _NavbarControllerService: NavbarControllerService) {
        this._NavbarControllerService.__Observable_Component
            .subscribe((rs) => {
                this.navBarComponent = rs;
            });
    }

    ngOnInit(): void {
    }

}
