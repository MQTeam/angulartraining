import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainLayoutComponent } from './main-layout/main-layout.component';
import { HeaderLayoutComponent } from './header-layout/header-layout.component';
import { NavLayoutComponent } from './nav-layout/nav-layout.component';
import {RouterModule} from '@angular/router';



@NgModule({
  declarations: [MainLayoutComponent, HeaderLayoutComponent, NavLayoutComponent],
  imports: [
    CommonModule,
    RouterModule
  ]
})
export class LayoutModule { }
